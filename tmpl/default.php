<?php
/*
 * default.php - mod_yt_weather - YT Weather by YesTheme
 * ------------------------------------------------------------------------
 *   Author     YesTheme http://yestheme.com
 *   Copyright (C) 2008 - 2013 YesTheme.com. All Rights Reserved.
 *   @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 *   Websites   http://yestheme.com
 * ------------------------------------------------------------------------
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

?>
<div id="yt-weather<?php echo $module->id; ?>"
     class="yt-weather<?php if ($params->get('moduleclass_sfx')) echo ' ' . $params->get('moduleclass_sfx'); ?><?php echo ($enable_float) ? " yt-weather-float" : ""; ?>">
    <?php if ($enable_float): ?>
        <div class="yt-weather-float-icon">
            <img src="<?php echo JURI::base(); ?>modules/mod_yt_weather/assets/img/weather-float-icon.png" alt="Weather module for Joomla by YesTheme.com" />
        </div>
    <?php endif; ?>
    <div class="yt-weather-main">
        <?php
        if (!function_exists('curl_init')) {
            echo JText::_('YT_CURL_ERROR');
            return;
        }
        $yt_home = base64_decode($params->get('yt_home'));
        $ytHelper = new modYTWeatherHelper($params, $module->id);

        if ($ytHelper->cache_enabled) {
            $ytWeoid = $ytHelper->cache->call(array($ytHelper, 'getWoeid'));
        } else {
            $ytWeoid = $ytHelper->getWoeid();
        }

        if ($ytWeoid != false) {
            $weatherData = $ytHelper->getWeatherData();
            if ($weatherData != false) {
                $locationTitle = (trim($params->get('translated_location')) == '') ? $params->get('location') : $params->get('translated_location');
                $forecastLocation = null;
                if ($params->get('show_forecast')) {
                    $forecastLocation = explode('_', $weatherData['query']['results']['rss']['channel']['item']['guid']['content']);
                    $forecastLocation = $forecastLocation[0];
                }
                $weatherData = $weatherData['query']['results']['rss']['channel'];
                $nIDs = array(27, 29, 31, 33);
                $at = in_array((int)$weatherData['item']['condition']['code'], $nIDs, true) ? 'n' : 'd';
                $icon = sprintf($iconURL, $weatherData['item']['condition']['code'], $at);
                ?>
                <div class="yt-current-weather">
                    <div class="yt-current-weather-icon">
                        <img class="yt-current-weather-big-icon" src="<?php echo $icon; ?>"
                             alt="<?php echo $ytHelper->t2l($weatherData['item']['condition']['text']); ?>" title="<?php echo $ytHelper->t2l($weatherData['item']['condition']['text']); ?>"/>
                    </div>
                    <div class="yt-current-weather-details">
                        <?php if ($params->get('show_location') == 1): ?>
                            <div class="yt-location">
                                <?php echo $locationTitle; ?>
                            </div>
                        <?php endif; ?>
                        <div class="yt-temp">
                            <?php if ($params->get('unit') == 'c') : ?>
                                <div class="yt-temp-value"><?php echo $weatherData['item']['condition']['temp']; ?></div>
                                <div class="yt-temp-unit"><?php echo JText::_('YT_WEATHER_C'); ?></div>
                            <?php else: ?>
                                <div class="yt-temp-value"><?php echo $weatherData['item']['condition']['temp']; ?></div>
                                <div class="yt-temp-unit"><?php echo JText::_('YT_WEATHER_F'); ?></div>
                            <?php endif; ?>
                            <br style="clear:both;"/>
                        </div>
                        <?php if ($params->get('show_condition') == 1): ?>
                            <div class="yt-condition">
                            <span
                                class="yt-condition-value"><?php echo $ytHelper->t2l($weatherData['item']['condition']['text']); ?></span>
                            </div>
                        <?php endif; ?>
                        <?php if ($params->get('show_humidity') == 1): ?>
                            <div class="yt-humidity">
                                <span class="yt-humidity-title"><?php echo JText::_('YT_WEATHER_HUMIDITY');  ?></span>:
                            <span
                                class="yt-humidity-value"><?php echo $ytHelper->n2l($weatherData['atmosphere']['humidity']); ?>
                                %</span>
                            </div>
                        <?php endif; ?>
                        <?php if ($params->get('show_wind') >= 1): ?>
                            <?php
                            $compass = array('N', 'NNE', 'NE', 'ENE', 'E', 'ESE', 'SE', 'SSE', 'S', 'SSW', 'SW', 'WSW', 'W', 'WNW', 'NW', 'NNW', 'N');
                            $weatherData['wind']['direction'] = $compass[round($weatherData['wind']['direction'] / 22.5)];
                            ?>
                            <div class="yt-wind">
                <span class="yt-wind-title">
                    <?php echo JText::_('YT_WEATHER_WIND');  ?>
                </span>:
                <span class="yt-wind-value">
                    <?php
                    if ($params->get('show_wind') == 1) {
                        $windToShow = JText::_($weatherData['wind']['direction']);
                    } else {
                        if (file_exists($wind_icon_dir . $weatherData['wind']['direction'] . '.gif')) {
                            $windToShow = '<img src="' . $wind_icon_url . $weatherData['wind']['direction'] . '.gif" alt="' . JText::_($weatherData['wind']['direction']) . '" title="' . JText::_($weatherData['wind']['direction']) . '" />';
                        }
                    }
                    ?>
                    <?php echo $windToShow . JText::_('YT_WEATHER_AT') . $ytHelper->n2l($weatherData['wind']['speed']) . ' ' . JText::_(strtoupper($weatherData['units']['speed']));  ?>
                </span>
                            </div>
                        <?php endif; ?>
                    </div>
                    <br style="clear:both;"/>
                </div>
                <br style="clear:both;"/>
            <?php
            }
            if ($forecastLocation != null) {
                $forecastData = $ytHelper->getForecastData($forecastLocation);
                if ($forecastData != false) {
                    ?>
                    <div class="yt-weather-forecast">
                        <?php
                        if ($params->get('show_forecast_days', 1)>=5)
                        {
                            $params->set('show_forecast_days', 4);
                        }
                        for ($i = 0; $i < $params->get('show_forecast_days', 1); $i++) {
                            $forecast = $forecastData[$i];
                            $divWidth = 100 / $params->get('show_forecast_days', 1);
                            $at = in_array((int)$forecast['code'], $nIDs, true) ? 'n' : 'd';
                            $icon = sprintf($iconURL, $forecast['code'], $at);
                            ?>
                            <div
                                class="yt-weather-forecast-details yt-weather-forecast-<?php echo ($i % 2 ? 'even' : 'odd') ?>"
                                style="width: <?php echo $divWidth; ?>%;">
                                <div class="yt-weather-forecast-date"><?php echo ($i==0) ? "Today" : $ytHelper->t2l($forecast['day']); ?></div>
                                <div class="yt-weather-forecast-icon">
                                    <img class="yt-weather-forecast-small-icon" src="<?php echo $icon; ?>"
                                         alt="<?php echo $ytHelper->t2l($forecast['text']); ?>"
                                         title="<?php echo $ytHelper->t2l($forecast['text']); ?>"/>
                                </div>
                                <div class="yt-weather-forecast-temp">
                                    <?php if ($params->get('unit') == 'c') : ?>
                                        <?php echo $forecast['low'] . JText::_('YT_WEATHER_C'); ?> | <?php echo $forecast['high'] . JText::_('YT_WEATHER_C'); ?>
                                    <?php else: ?>
                                        <?php echo $forecast['low'] . JText::_('YT_WEATHER_F'); ?> | <?php echo $forecast['high'] . JText::_('YT_WEATHER_F'); ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                        <?php
                        }
                        ?>
                        <br style="clear:both;"/>
                    </div>
                <?php
                } else {
                    echo JText::_('YT_FORECAST_ERROR');
                }
            }
        } else {
            echo JText::_('YT_LOCATION_ERROR');
        }
        echo $yt_home;
        ?>
    </div>
