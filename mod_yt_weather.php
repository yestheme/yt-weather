<?php
/*
 * mod_yt_weather.php - mod_yt_weather - YT Weather by YesTheme
 * ------------------------------------------------------------------------
 *   Author     YesTheme http://yestheme.com
 *   Copyright (C) 2008 - 2013 YesTheme.com. All Rights Reserved.
 *   @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 *   Websites   http://yestheme.com
 * ------------------------------------------------------------------------
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

if (!defined('DS')) {
    define('DS', DIRECTORY_SEPARATOR);
}

require_once (dirname(__FILE__) . DS . 'helper.php');

JHTML::_('behavior.framework');
$document = JFactory::getDocument();

$checkJqueryLoaded = false;
$header = $document->getHeadData();


// Params
$moduleclass_sfx = $params->get('moduleclass_sfx', '');
$enable_float = $params->get('enable_float', '1');

if ($enable_float) {

    foreach($header['scripts'] as $scriptName => $scriptData)
    {
        if(substr_count($scriptName,'/jquery')){
            $checkJqueryLoaded = true;
        }
    }
    if(!$checkJqueryLoaded) {
        $document->addScript(JURI::root().'modules/mod_yt_weather/assets/js/jquery.min.js');
    }
    $document->addScript(JURI::root().'modules/mod_yt_weather/assets/js/jquery-ui-1.10.3.custom.js');
    $document->addScript(JURI::root().'modules/mod_yt_weather/assets/js/mod_yt_weather.js');

    $floatScript = "
	jQuery(document).ready(function(){
        jQuery(\"#yt-weather".$module->id."\").hover(function(){
                jQuery(\"#yt-weather".$module->id."\").stop(true, false).animate({right: \"0\" }, 500, 'easeOutQuint' );
            },
            function(){
                jQuery(\"#yt-weather".$module->id."\").stop(true, false).animate({right: \"-400\" }, 500, 'easeInQuint' );
            },1000);
    });";
    $document->addScriptDeclaration($floatScript);
}

$document->addStylesheet(JURI::base(true) . '/modules/mod_yt_weather/assets/css/mod_yt_weather.css');
$wind_icon_dir = dirname(__FILE__) . DS . 'assets' . DS . 'img' . DS . 'wind' . DS . $params->get('wind_icon_styles', 'default') . DS;
$wind_icon_url = JURI::base(true) . '/modules/mod_yt_weather/assets/img/wind/' . $params->get('wind_icon_styles', 'default') . '/';
if ($params->get('condition_image_style', 'default')=='weather_dot_com')
{
    $iconURL = 'http://s.imwx.com/v.20120328.084208/img/wxicon/120/%d.png';
}
elseif ($params->get('condition_image_style', 'default')=='yahoo_dot_com')
{
    $iconURL = 'http://l.yimg.com/os/mit/media/m/weather/images/icons/l/%d%s-100567.png';
}
else
{
    $iconURL = JURI::base(true).'/modules/mod_yt_weather/assets/img/condition/ytdefault/%d.png';
}
require (JModuleHelper::getLayoutPath('mod_yt_weather', 'default'));