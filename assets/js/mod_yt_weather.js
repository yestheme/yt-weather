/*
 * mod_yt_weather.js - mod_yt_weather - YT Weather by YesTheme
 * ------------------------------------------------------------------------
 *   Author     YesTheme http://yestheme.com
 *   Copyright (C) 2008 - 2013 YesTheme.com. All Rights Reserved.
 *   @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 *   Websites   http://yestheme.com
 * ------------------------------------------------------------------------
 */
jQuery.noConflict();