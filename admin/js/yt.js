/*
 * yt.js - mod_yt_float_social - YT Float Social by YesTheme
 * ------------------------------------------------------------------------
 *   Author     YesTheme http://yestheme.com
 *   Copyright (C) 2008 - 2013 YesTheme.com. All Rights Reserved.
 *   @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 *   Websites   http://yestheme.com
 * ------------------------------------------------------------------------
 */

jQuery.noConflict();
window.addEvent("domready",function(){
    jQuery("#jform_params_assets-lbl").parents("li:first").remove();
    if (jQuery("#jform_params_enable_facebook1").attr('checked')=='checked') {
        jQuery('#facebook-options').parents('div.panel').slideUp();
    }
    if (jQuery("#jform_params_enable_twitter1").attr('checked')=='checked') {
        jQuery('#twitter-options').parents('div.panel').slideUp();
    }
    if (jQuery("#jform_params_enable_pinterest1").attr('checked')=='checked') {
        jQuery('#pinterest-options').parents('div.panel').slideUp();
    }

    jQuery("#jform_params_enable_facebook0").bind('click', function(){
        jQuery('#facebook-options').parents('div.panel').slideDown();
    });
    jQuery("#jform_params_enable_facebook1").bind('click', function(){
        jQuery('#facebook-options').parents('div.panel').slideUp();
    });

    jQuery("#jform_params_enable_twitter0").bind('click', function(){
        jQuery('#twitter-options').parents('div.panel').slideDown();
    });
    jQuery("#jform_params_enable_twitter1").bind('click', function(){
        jQuery('#twitter-options').parents('div.panel').slideUp();
    });

    jQuery("#jform_params_enable_pinterest0").bind('click', function(){
        jQuery('#pinterest-options').parents('div.panel').slideDown();
    });
    jQuery("#jform_params_enable_pinterest1").bind('click', function(){
        jQuery('#pinterest-options').parents('div.panel').slideUp();
    });
});