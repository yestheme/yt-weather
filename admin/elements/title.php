<?php
/*
 * title.php - mod_yt_float_social - YT Float Social by YesTheme
 * ------------------------------------------------------------------------
 *   Author     YesTheme http://yestheme.com
 *   Copyright (C) 2008 - 2013 YesTheme.com. All Rights Reserved.
 *   @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 *   Websites   http://yestheme.com
 * ------------------------------------------------------------------------
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.form.formfield');

class JFormFieldTitle extends JFormField {

    protected $type = 'Title';

    protected function getInput() {

    }

    protected function getLabel() {
        return "<h2 class=\"yt-title-label\">".JText::_($this->element['label'])."</h2>";
    }
}
?>