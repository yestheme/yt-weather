<?php
/*
 * helper.php - mod_yt_weather - YT Weather by YesTheme
 * ------------------------------------------------------------------------
 *   Author     YesTheme http://yestheme.com
 *   Copyright (C) 2008 - 2013 YesTheme.com. All Rights Reserved.
 *   @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 *   Websites   http://yestheme.com
 * ------------------------------------------------------------------------
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

class modYTWeatherHelper
{
    public $params;
    public $moduleId;
    public $ytWoeid;
    public $mDir;
    public $cache;
    public $cache_enabled;
    public $cache_time;
    public $location = array();

    public function __construct($params, $id)
    {
        $this->params = $params;
        $this->moduleId = $id;
        $this->cache_time = (int)$this->params->get('cache_time', 900);
        $this->cache_enabled = $this->params->get('cache', 0) ? true : false;
        $this->cache = & JFactory::getCache('mod_yt_weather');
        $this->cache->setCaching($this->cache_enabled);
        $this->cache->setLifeTime($this->cache_time);
        if ($this->cache_enabled) {
            $this->cache->call(array($this, 'setWoeid'));
        } else {
            $this->setWoeid();
        }
        $this->setWoeid();
        $this->mDir = basename(dirname(__FILE__));
    }

    public function ytYQL($q, $p = array('format' => 'json'))
    {
        $yql = 'http://query.yahooapis.com/v1/public/yql?q=';
        $query = rawurlencode($q);
        $yql .= str_replace('%2A', '*', $query);
        $yql .= '&' . http_build_query($p, '', '&');
        return $yql;
    }

    public function setWoeid()
    {
        $yql = "select * from geo.places(1) where text='" . $this->params->get('location') . "'";
        $url = $this->ytYQL($yql);
        $d = $this->ytCurl($url);
        $d = json_decode($d, true);
        $this->ytWoeid = $d['query']['results']['place']['woeid'];
        $this->location = $d['query']['results']['place'];
    }

    public function getWoeid()
    {
        return $this->ytWoeid;
    }

    public function ytCurl($url, $q = array())
    {
        $rURL = $url;

        if (!empty($q) and is_array($q)) $rURL .= '?' . http_build_query($q, '', '&');

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($curl, CURLOPT_URL, $rURL);
        curl_setopt($curl, CURLOPT_TIMEOUT, 60);
        curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
        if (strtolower(substr($rURL, 0, 5)) === 'https') {
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
        }
        $d = curl_exec($curl);
        $e = trim(curl_error($curl));
        curl_close($curl);
        if (!empty($e)) {
            return false;
        }
        return $d;
    }

    function getWeatherData()
    {
        $q = 'select * from xml where url="http://weather.yahooapis.com/forecastrss?w=' . $this->ytWoeid . '&u=' . $this->params->get('unit') . '"';
        if ($this->cache_enabled) {
            $u = $this->cache->call(array($this, 'ytYQL'), $q);
            $d = $this->cache->call(array($this, 'ytCurl'), $u);
        } else {
            $u = $this->ytYQL($q);
            $d = $this->ytCurl($u);
        }
        if ($d != false) {
            return json_decode($d, true);
        } else {
            return false;
        }
    }

    function getForecastData($location)
    {

        $q = 'SELECT * FROM rss WHERE url="http://xml.weather.yahoo.com/forecastrss/' . $location . '_' . strtolower($this->params->get('unit')) . '.xml"';
        if ($this->cache_enabled) {
            $u = $this->cache->call(array($this, 'ytYQL'), $q);
            $d = $this->cache->call(array($this, 'ytCurl'), $u);
        } else {
            $u = $this->ytYQL($q);
            $d = $this->ytCurl($u);
        }
        if ($d != false) {
            $d = json_decode($d, true);
            return $d['query']['results']['item']['forecast'];
        } else {
            return false;
        }
    }

    public function t2l($t)
    {
        $tr = array("/" => "_", " " => "_", ')' => '', "(" => "");
        $t = strtr($t, $tr);
        return JText::_('YT_WEATHER_' . strtoupper($t));
    }

    public function n2l($n, $p = 'YT_')
    {
        $n = (array)str_split($n);
        $f = "";
        foreach ($n as $no) {
            if (ctype_digit($no)) {
                $f .= JText::_($p . $no);
            } else {
                $f .= $no;
            }
        }
        return $f;
    }

    public function cu($v, $u)
    {
        $t = $this->n2l($v);
        $t .= (strtolower($u) == 'f') ? JText::_('YT_WEATHER_' . 'F') : JText::_('YT_WEATHER_' . 'C');
        return $t;
    }
}