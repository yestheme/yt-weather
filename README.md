YT Weather - Joomla! Weather Forecast Module
===

YT Weather is simple and one of the best weather forecast module for Joomla! This module developed by YesTheme and using Yahoo API to display the weather data for current day and weather forecast next 5 days. The temperature can be displayed in Celsius and Fahrenheit. The wind can be displayed in text mode or graphic mode. This module is very useful for your website, it is easy to install, config and use!

Compatibility:
---
- Joomla! 3.0
- Joomla! 2.5
- Joomla! 1.7/1.6/1.5

Features:
---
- Display the current day weather information
- Display weather forecast information
- Translate the location name to display
- Show/Hide: Location, Condition, Humidity, Wind Infomation
- Display wind direction in text mode or graphic mode.
- Use Yahoo API to retrieve data
- Display weather forecast information
- Easy to choice number of the day you want to show the forecast information
- Weather data can be translated into your language
- Joomla! Cache Support.

####Version

**1.0.4**

------------- [YT Weather Version 1.0.4 - August 03 2013] -------------

- Fix float function doesn't work on Joomla 3.x

----

**1.0.3**

------------- [YT Weather Version 1.0.3 - August 03 2013] -------------

- Fix installation error on joomla 3.x

----

**1.0.2**

------------- [YT Weather Version 1.0.2 - July 31 2013] -------------

- Add German Translation (Special thank to great customer from Germany)
- Add missing language entries for AM & PM Showers/Wind, Partly cloudy/windy
- Add new style in options page for joomla 2.5.
- Add float option

----

**1.0.1**

------------- [YT Weather Version 1.0.1 - May 09 2013] -------------

- Fix high and low forecast temperature
- Fix main weather condition image alt and title
- Fix weather forecast data (Max forecast days: 4)
- Add weather condition image style (Default, Weather.com, Yahoo.com)

----

**1.0.0**

-------------------- [YT Weather Version 1.0.0] --------------------

Initial release of YT Weather

----

####Install and use
>Download the module, install to your Joomla! Site and publish it to any position you want to show it!

License
-
GNU/GPL